---
title: "PS6 2018-2019"
draft: false
---

# Projet de semestre 6 / 2018-2019

## Liens utiles

* [Directives PS6 2018 - 2019](https://drive.switch.ch/index.php/s/RraU71k8rfKTYp2)
* [Guides pour la rédaction d'articles scientifiques](https://drive.switch.ch/index.php/s/82mTMj3f9x5730c)
