---
title: "à propos"
image: "jacques_supcik.jpg"
weight: 0
---

Je suis professeur en télécommunications à la "[Haute école d'ingénierie et d'architecture](https://www.heia-fr.ch/) " de Fribourg. Pendant mon temps libre, j'aime écrire des programmes informatiques, apprendre de nouvelles choses et de bricoler avec la technologie, mais une fois par semaine, je sors avec mon cheval pour une longue [promenade](https://www.facebook.com/centre.equestre.corminboeuf/) dans la forêt.

Mon langage de programmation préféré est [go](https://golang.org/) et j'aime aussi coder en Python et Kotlin. J'enseigne Java pour les étudiants de première année (Introduction à la programmation), le C et assembleur ARM pour les étudiants de deuxième année (Systèmes embarqués). Je donne également un cours sur les systèmes d'exploitation.
