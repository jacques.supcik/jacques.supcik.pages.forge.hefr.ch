---
title: "Contact"
draft: false
---
<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
Haute école d'ingénierie et d'architecture,
Jacques Supcik,
bd de Pérolles 80,
1700 Fribourg,
Switzerland
