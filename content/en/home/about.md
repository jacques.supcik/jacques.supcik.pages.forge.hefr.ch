---
title: "About"
image: "jacques_supcik.jpg"
weight: 0
---

I am a full time professor in telecommunications at the "[Haute école d'ingénierie et d'architecture](https://www.heia-fr.ch/)" of Fribourg. In my free time, I continue to write code, learn things and tinker with technology, but once a week, I go out with my horse for a long [ride](https://www.facebook.com/centre.equestre.corminboeuf/) in the forrest.

My favorite programming language is [go](https://golang.org/) and I also like to code in Python and Kotlin. I am teaching Java for the students in the first year (Introduction to programming) and C and ARM assembler for the students in the second year (Embedded systems). I also give a lecture on Operating Systems.
